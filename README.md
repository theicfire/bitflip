# Performance Engineering

One of my favorite classes I took at MIT was 6.127 - performance engineering. And probably my favorite project in the class was the first one. The problem statement is simple: flip a series of bits. That is, if the input is 2 bytes of `0xfac1` == `0b1111101011000001`, the flipped version is `0x835f` == `0b1000001101011111`.

But you have to do this as fast as possible :)

Check out the pdf for the actual problem set. You don't really have to read it though.

I've supplied you with some code. Clone it, and make sure the tests pass:

```
$ make test
```

The tests are in the `tests/` directory. You can run individual tests by running
```
$ make
$ ./everybit -t tests/default
```

I've added enough tests for you that you won't have to write any. They should be comprehensive enough. Just run `make test` to verify correctness.

The challenge: make `./everybit -r` run as fast as possible. You'll notice right now it's so slow it doesn't even complete. You can decrease the number of bytes to flip in `longrunning_rotation` in `tests.c` to see this finish.

See how fast you can get it (hint: it should be very very fast). Email me (chaselambda@gmail.com) when you think you've got it fast. I'll let you know what the benchmark was for full performance marks. Then you can try and make it go faster, knowing this.

This took me on the order of 40-50 hours to complete. I also had a partner. So embrace yourself if you're ready for the challenge, and see if you can get someone else to work on this with you! One neat thing about this is each hour you put in you learn more and make things go faster. Even the first 10 hours will hopefully be rewarding!

### Resources
Check out the OCW class: http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-172-performance-engineering-of-software-systems-fall-2010/video-lectures/

The videos are excellent. Make sure to download them on VLC and use the speed up/slow down shortcut keys, as you should do on all videos :)
